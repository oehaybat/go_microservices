package handlers

import "haybat.org/go_microservices/data"

// A list of products in the response
// swagger:response productsResponse
type productsResponseWrapper struct {
	// All products in the system
	// in: body
	Body []data.Product
}

// swagger:parameters deleteProduct
type productIdParameterWrapper struct {
	// The id of the product to delete from the database
	// in: path
	// required: true
	Id int `json: "id"`
}

// swagger:response noContent
type productNoContentWrapper struct {
}
