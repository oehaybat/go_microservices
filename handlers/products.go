// Package classification of Product API
//
// # Documentation for Product API
//
// Schemes: http
// BasePath: /
// Version: 1.0.0
//
// Consumes:
// - application/json
//
// Produces:
// - application/json
// swagger:meta
package handlers

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"haybat.org/go_microservices/data"
	"log"
	"net/http"
	"strconv"
)

type Products struct {
	l *log.Logger
}

func NewProducts(l *log.Logger) *Products {
	return &Products{l}
}

// swagger:route GET /products products listProducts
// Returns a list of products
// responses:
// 200: productsResponse

// GetProducts returns the products from the data store
func (p *Products) GetProducts(rw http.ResponseWriter, request *http.Request) {
	p.l.Println("Handle GET product")

	rw.Header().Add("Content-Type", "application/json")

	productList := data.GetProducts()
	err := productList.ToJSON(rw)
	if err != nil {
		http.Error(rw, "Unable to encode json", http.StatusInternalServerError)
	}
}

func (p *Products) AddProduct(rw http.ResponseWriter, request *http.Request) {
	p.l.Println("Handle POST product")

	prod := request.Context().Value(ContextKeyProduct{}).(data.Product)
	data.AddProduct(&prod)

	p.l.Printf("Prod: %#v", prod)
}

func (p *Products) UpdateProduct(rw http.ResponseWriter, request *http.Request) {
	p.l.Println("Handle PUT product")

	requestVars := mux.Vars(request)
	id, err := strconv.Atoi(requestVars["id"])

	if err != nil {
		http.Error(rw, "Unable to convert product id", http.StatusBadRequest)
		return
	}

	prod := request.Context().Value(ContextKeyProduct{}).(data.Product)

	err = data.UpdateProduct(id, &prod)
	if err == data.ErrorProductNotFound {
		http.Error(rw, "Product not found", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, "Unable to update product", http.StatusInternalServerError)
		return
	}
}

// swagger:route DELETE /products/{Id} products deleteProduct
// Deletes the product with the given id
// responses:
//  201: noContent

// DeleteProduct deletes a product from the database
func (p *Products) DeleteProduct(rw http.ResponseWriter, request *http.Request) {
	p.l.Println("Handle DELETE product")

	requestVars := mux.Vars(request)
	id, err := strconv.Atoi(requestVars["id"])

	if err != nil {
		http.Error(rw, "Unable to convert product id", http.StatusBadRequest)
		return
	}

	prod := request.Context().Value(ContextKeyProduct{}).(data.Product)

	data.DeleteProduct(id, &prod)
}

type ContextKeyProduct struct {
}

// MiddlewareValidateProduct validates the product in the request and calls next if ok
func (p *Products) MiddlewareValidateProduct(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		prod := data.Product{}

		err := prod.FromJSON(r.Body)
		if err != nil {
			p.l.Println("[ERROR] deserializing product", err)
			http.Error(rw, "Error reading product", http.StatusBadRequest)
			return
		}

		// validate the product
		err = prod.Validate()
		if err != nil {
			p.l.Println("[ERROR] validating product", err)
			http.Error(
				rw,
				fmt.Sprintf("Error validating product: %s", err),
				http.StatusBadRequest,
			)
			return
		}

		// add product to context
		ctx := context.WithValue(r.Context(), ContextKeyProduct{}, prod)
		req := r.WithContext(ctx)

		next.ServeHTTP(rw, req)
	})
}
