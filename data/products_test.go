package data

import "testing"

func TestCheckValidation(t *testing.T) {
	p := &Product{}
	p.Name = "required"
	p.Price = 1
	p.SKU = "xxxx-xxxx-xx"

	err := p.Validate()
	if err != nil {
		t.Fatal(err)
	}
}
